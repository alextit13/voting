package com.elections.voting.model.database

import android.content.Context
import com.elections.voting.model.main.ApplicationProvider
import com.elections.voting.model.models.VoteData
import com.google.gson.Gson

const val SP_DATABASE = "sp_database"
const val SP_USER = "sp_user"
const val SP_ADMIN = "sp_admin"
const val SP_NEW_VOTE = "sp_new_vote"
const val SP_NUM_VOTE_MAN = "sp_num_vote_man"
const val SP_NEW_NAME_VOTE = "sp_new_name_vote"

fun saveUserToPreferences() {
    ApplicationProvider.currentContext?.getSharedPreferences(SP_DATABASE, Context.MODE_PRIVATE)
        ?.edit()
        ?.putBoolean(SP_USER, true)
        ?.apply()
}

fun checkSavedUser(): Boolean? {
    return ApplicationProvider.currentContext?.getSharedPreferences(SP_DATABASE, Context.MODE_PRIVATE)
        ?.getBoolean(SP_USER, false)
}

fun deleteUserFromPreferences() {
    ApplicationProvider.currentContext?.getSharedPreferences(SP_DATABASE, Context.MODE_PRIVATE)
        ?.edit()
        ?.putBoolean(SP_USER, false)
        ?.apply()
}

fun saveAdmin() {
    ApplicationProvider.currentContext?.getSharedPreferences(SP_DATABASE, Context.MODE_PRIVATE)
        ?.edit()
        ?.putBoolean(SP_ADMIN, true)
        ?.apply()
}

fun checkAdmin(): Boolean? {
    return ApplicationProvider.currentContext?.getSharedPreferences(SP_DATABASE, Context.MODE_PRIVATE)
        ?.getBoolean(SP_ADMIN, false)
}

fun deleteAdmin() {
    ApplicationProvider.currentContext?.getSharedPreferences(SP_DATABASE, Context.MODE_PRIVATE)
        ?.edit()
        ?.putBoolean(SP_ADMIN, false)
        ?.apply()
}

fun saveNewVote(voteData: VoteData) {
    ApplicationProvider.currentContext?.getSharedPreferences(SP_DATABASE, Context.MODE_PRIVATE)
        ?.edit()
        ?.putString(SP_NEW_VOTE, Gson().toJson(voteData))
        ?.apply()
}

fun saveNewNameVote(nameVote: String) {
    ApplicationProvider.currentContext?.getSharedPreferences(SP_DATABASE, Context.MODE_PRIVATE)
        ?.edit()
        ?.putString(SP_NEW_NAME_VOTE, nameVote)
        ?.apply()
}

fun getCurrentVote(): VoteData? {
    return Gson().fromJson<VoteData>(ApplicationProvider.currentContext?.getSharedPreferences(SP_DATABASE, Context.MODE_PRIVATE)
            ?.getString(SP_NEW_VOTE, ""), VoteData::class.java)
}

fun getNameCurrentVote(): String {
    return ApplicationProvider.currentContext?.getSharedPreferences(SP_DATABASE, Context.MODE_PRIVATE)
        ?.getString(SP_NEW_NAME_VOTE, "")!!
}

fun deleteVotingFromDevice() {
    ApplicationProvider.currentContext?.getSharedPreferences(SP_DATABASE, Context.MODE_PRIVATE)
        ?.edit()
        ?.putString(SP_NEW_VOTE, "")
        ?.apply()
    ApplicationProvider.currentContext?.getSharedPreferences(SP_DATABASE, Context.MODE_PRIVATE)
        ?.edit()
        ?.putString(SP_NEW_NAME_VOTE, "")
        ?.apply()
}

fun addVoteMan() {
    val nums = getAllVoteMan()?.plus(1)
    nums?.let {
        ApplicationProvider.currentContext?.getSharedPreferences(SP_DATABASE, Context.MODE_PRIVATE)
            ?.edit()
            ?.putInt(SP_NUM_VOTE_MAN, it)
            ?.apply()
    }
}

fun getAllVoteMan(): Int? {
    return ApplicationProvider.currentContext?.getSharedPreferences(SP_DATABASE, Context.MODE_PRIVATE)
        ?.getInt(SP_NUM_VOTE_MAN, 0)
}

fun clearAllVoteMan() {
    ApplicationProvider.currentContext?.getSharedPreferences(SP_DATABASE, Context.MODE_PRIVATE)
        ?.edit()
        ?.putInt(SP_NUM_VOTE_MAN, 0)
        ?.apply()
}