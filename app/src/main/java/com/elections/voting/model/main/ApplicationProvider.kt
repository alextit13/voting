package com.elections.voting.model.main

import android.app.Application
import android.content.Context
import com.google.firebase.FirebaseApp

class ApplicationProvider : Application() {
    companion object {
        var currentContext: Context? = null
    }

    override fun onCreate() {
        super.onCreate()
        currentContext = applicationContext
    }
}