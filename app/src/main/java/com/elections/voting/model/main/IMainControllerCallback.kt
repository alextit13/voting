package com.elections.voting.model.main

import com.elections.voting.model.models.VoteData

interface IMainControllerCallback {
    fun onDataFromPlot(data: VoteData)
    fun onListPlotsResult(list: MutableList<String>)
}