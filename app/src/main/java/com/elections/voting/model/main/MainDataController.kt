package com.elections.voting.model.main

import com.elections.voting.model.database.getNameCurrentVote
import com.elections.voting.model.models.VoteData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.util.*

class MainDataController(val callback: IMainControllerCallback) {

    fun getMainData() {
        FirebaseDatabase.getInstance().reference
            .child("main")
            .child(getNameCurrentVote())
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(databaseError: DatabaseError) {

                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    currentDataValue.clear()
                    for (data in dataSnapshot.children) {
                        if (data.key != "test") {
                            val currentListData = mutableListOf<VoteData>()
                            val cldm = data.value as HashMap<String, VoteData>
                            currentListData.addAll(cldm.values)
                            currentDataValue[data.key!!] = currentListData
                        }
                    }
                    val list: MutableList<String> = mutableListOf()
                    list.addAll(currentDataValue.keys)
                    list.sort()
                    callback.onListPlotsResult(list)
                }
            })
    }

    fun getDataForAllPlots() {
        FirebaseDatabase.getInstance().reference.child("main")
            .child(getNameCurrentVote())
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val listVoteData: MutableList<VoteData> = mutableListOf()
                    for (data in dataSnapshot.children) {
                        for (dataVisor in data.children) {
                            if (dataVisor.key != "test")
                                try {
                                    listVoteData.add(dataVisor.getValue(VoteData::class.java)!!)
                                } catch (e: java.lang.Exception) {
                                }
                        }
                    }
                    if (listVoteData.isNotEmpty())
                        calculateData(listVoteData)
                }
            })
    }

    fun getDataFromPlot(namePlot: String) {
        FirebaseDatabase.getInstance().reference.child("main")
            .child(getNameCurrentVote())
            .child(namePlot)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val listMaps: MutableList<VoteData> = mutableListOf()
                    for (data in dataSnapshot.children) {
                        if (data.key != "test")
                            listMaps.add(data.getValue(VoteData::class.java)!!)
                    }
                    if (listMaps.isNotEmpty())
                        calculateData(listMaps)
                }
            })
    }

    fun getDataForSelectedPlot(listPlots: MutableList<String>) {
        val listMaps: MutableList<VoteData> = mutableListOf()
        var countIteration = 0
        listPlots.forEach {
            FirebaseDatabase.getInstance().reference.child("main")
                .child(getNameCurrentVote())
                .child(it)
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        countIteration++
                        for (data in dataSnapshot.children) {
                            if (data.key != "test")
                                listMaps.add(data.getValue(VoteData::class.java)!!)
                        }
                        if (listMaps.isNotEmpty() && countIteration == listPlots.size)
                            calculateData(listMaps)
                    }
                })
        }
    }

    private fun calculateData(listMaps: MutableList<VoteData>) {
        val voteData = VoteData()

        var s_2 = 0f
        listMaps.forEach {
            s_2 += it.s_2.toInt()
        }
        voteData.s_2 = s_2.toString()

        var s_2_1 = 0f
        listMaps.forEach {
            s_2_1 += it.s_2_1.toFloat()
        }
        voteData.s_2_1 = s_2_1.toString()
        try {
            voteData.s_2_1 = voteData.s_3.substring(0, s_2_1.toString().indexOf("."))
        } catch (e: Exception) {
        }

        var s3 = 0f
        listMaps.forEach {
            s3 += it.s_3.toFloat()
        }
        voteData.s_3 = s3.toString()
        try {
            voteData.s_3 = voteData.s_3.substring(0, s3.toString().indexOf("."))
        } catch (e: Exception) {
        }


        var s3_1 = 0f
        listMaps.forEach {
            try {
                s3_1 += it.s_3_1.toFloat()
            } catch (e: java.lang.Exception) {
            }
        }
        voteData.s_3_1 = s3_1.toString()
        try {
            voteData.s_3_1 = voteData.s_3_1.substring(0, s3_1.toString().indexOf("."))
        } catch (e: Exception) {
        }


        var s4 = 0f
        listMaps.forEach {
            s4 += it.s_4.toFloat()
        }
        voteData.s_4 = s4.toString()
        try {
            voteData.s_4 = voteData.s_4.substring(0, s4.toString().indexOf("."))
        } catch (e: Exception) {
        }


        var s4_1 = 0f
        listMaps.forEach {
            try {
                s4_1 += it.s_4_1.toFloat()
            } catch (e: java.lang.Exception) {
            }
        }
        voteData.s_4_1 = s4_1.toString()
        try {
            voteData.s_4_1 = voteData.s_4_1.substring(0, s4_1.toString().indexOf("."))
        } catch (e: Exception) {
        }


        var s5 = 0f
        listMaps.forEach {
            try {
                s5 += it.s_5.toFloat()
            } catch (e: java.lang.Exception) {
            }
        }
        voteData.s_5 = s5.toString()
        try {
            voteData.s_5 = voteData.s_5.substring(0, s5.toString().indexOf("."))
        } catch (e: Exception) {
        }


        var s6 = 0f
        listMaps.forEach {
            try {
                s6 += it.s_6.toFloat()
            } catch (e: java.lang.Exception) {
            }
        }
        s6 /= listMaps.size
        voteData.s_6 = s6.toString()
        try {
            voteData.s_6 = voteData.s_6.substring(0, s6.toString().indexOf(".") + 3)
        } catch (e: Exception) {
        }


        var s7 = 0f
        listMaps.forEach {
            try {
                s7 += it.s_7.toFloat()
            } catch (e: java.lang.Exception) {
            }
        }
        s7 /= listMaps.size
        voteData.s_7 = s7.toString()
        try {
            voteData.s_7 = voteData.s_7.substring(0, s7.toString().indexOf(".") + 3)
        } catch (e: Exception) {
        }

        callback.onDataFromPlot(voteData)
    }

    companion object {
        val currentDataValue: HashMap<String, MutableList<VoteData>> = hashMapOf()
        var lastPlotsQuery: MutableList<String> = mutableListOf()
    }
}