package com.elections.voting.model.models

class Vote {
    var isVoteComplete = false
    var voteData: VoteData? = null
    var visors: MutableList<String> = mutableListOf()
    companion object {
        const val IS_VOTE_COMPLETE = "isVoteComplete"
    }
}