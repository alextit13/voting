package com.elections.voting.model.network.admin

import com.elections.voting.model.database.getNameCurrentVote
import com.google.firebase.database.FirebaseDatabase

class AdminNetworkProvider(private val callback: IAdminNetworkCallback) : IAdminNetworkProvider {

    override fun finishVoting() {
        callback.onFinishVoting()
    }

    override fun createVote(nameVote: String) {
        FirebaseDatabase.getInstance().reference.child("main")
            .child(nameVote)
            .child("test")
            .setValue(false)
    }

    override fun onVotingConsist(consist: Boolean) {
        FirebaseDatabase.getInstance().reference.child("main")
            .child(getNameCurrentVote())
            .child("test")
            .setValue(consist)
    }
}