package com.elections.voting.model.network.admin

interface IAdminNetworkCallback {
    fun onFinishVoting()
}