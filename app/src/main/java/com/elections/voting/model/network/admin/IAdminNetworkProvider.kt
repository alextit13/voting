package com.elections.voting.model.network.admin

interface IAdminNetworkProvider {
    fun finishVoting()
    fun createVote(nameVote: String)
    fun onVotingConsist(consist: Boolean)
}