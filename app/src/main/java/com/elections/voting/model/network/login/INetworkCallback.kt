package com.elections.voting.model.network.login

interface INetworkCallback {
    fun pairIsCorrect()
    fun pairIsIncorrect()
}