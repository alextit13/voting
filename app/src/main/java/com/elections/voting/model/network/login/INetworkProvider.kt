package com.elections.voting.model.network.login

interface INetworkProvider {
    fun checkPairLoginPass(login: String, password: String)
}