package com.elections.voting.model.network.login

import com.elections.voting.model.database.saveNewVote
import com.elections.voting.model.models.Visor.Companion.LOGIN
import com.elections.voting.model.models.Visor.Companion.PASS
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class NetworkProvider(val callback: INetworkCallback) : INetworkProvider {

    companion object {
        const val MAIN = "main"
        const val VOTINGS = "votings"
        const val VISORS = "visors"
        var SAVED_VOTE_NAME = ""
    }

    override fun checkPairLoginPass(login: String, password: String) {

    }
}