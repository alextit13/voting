package com.elections.voting.model.network.main

interface IMainDataCallback {
    fun onPushDataResult()
    fun onErrorPushData()
    fun onAllVotingsResult(list: MutableList<String>)
    fun onValidDataResult(value: Boolean?)
    fun onVoteWasClosed()
    fun onPushFirstDataResultOk()
}