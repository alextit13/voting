package com.elections.voting.model.network.main

import com.elections.voting.model.database.getCurrentVote
import com.elections.voting.model.database.getNameCurrentVote
import com.elections.voting.model.models.VoteData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class MainDataProvider {

    fun pushDataAboutUserToServer(currentVote: VoteData, callback: IMainDataCallback) {
        FirebaseDatabase.getInstance().reference.child("main")
            .child(getNameCurrentVote())
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    for (data in dataSnapshot.children) {
                        if (data.key == "test") {
                            if (data.getValue(Boolean::class.java)!!) {
                                FirebaseDatabase.getInstance().reference
                                    .child("main")
                                    .child(getNameCurrentVote())
                                    .child(currentVote.s_1)
                                    .child(currentVote.s_fio)
                                    .setValue(currentVote)
                                    .addOnCompleteListener {
                                        callback.onPushFirstDataResultOk()
                                    }
                            } else {
                                callback.onVoteWasClosed()
                            }
                        }
                    }
                }
            })
    }

    fun getAllVotings(callback: IMainDataCallback) {
        FirebaseDatabase.getInstance().reference.child("main")
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val listVotings: MutableList<String> = mutableListOf()
                    for (data in dataSnapshot.children) {
                        data.key?.let { listVotings.add(it) }
                    }

                    callback.onAllVotingsResult(listVotings)
                }
            })
    }

    fun checkConsistVoting(callback: IMainDataCallback) {
        FirebaseDatabase.getInstance().reference.child("main")
            .child(getNameCurrentVote())
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    for (data in dataSnapshot.children) {
                        if (data.key == "test")
                            callback.onValidDataResult(data.getValue(Boolean::class.java))
                    }
                }
            })
    }
}