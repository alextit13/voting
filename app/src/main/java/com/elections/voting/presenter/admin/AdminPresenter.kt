package com.elections.voting.presenter.admin

import com.elections.voting.model.database.*
import com.elections.voting.model.network.admin.AdminNetworkProvider
import com.elections.voting.model.network.admin.IAdminNetworkCallback
import com.elections.voting.model.network.admin.IAdminNetworkProvider
import com.elections.voting.view.admin.IAdminView

class AdminPresenter() : IAdminPresenter, IAdminNetworkCallback {

    private var view: IAdminView? = null
    private var provider: IAdminNetworkProvider? = null

    override fun onViewAttach(view: IAdminView) {
        this.view = view
        view.initViews()
        view.initListeners()

        if (getNameCurrentVote() != "")
            view.setNameOfCurrentVoteInTextView(getNameCurrentVote())

        if (provider == null)
            provider = AdminNetworkProvider(this)
    }

    override fun onClickCreateVote() {
        view?.showDialogCreateVote()
    }

    override fun onClickStartVoting() {
        provider?.onVotingConsist(true)
        view?.showToast("Голосование начато")
    }

    override fun onClickCreateVoteWithName(nameVote: String) {
        if (nameVote.isNotEmpty()) {
            provider?.createVote(nameVote)
            saveNewNameVote(nameVote)
            view?.showToast("Голосование создано")
            view?.setNameOfCurrentVoteInTextView(nameVote)
        }
    }

    override fun onClickFinishVotingButton() {
        provider?.finishVoting()
    }

    override fun onFinishVoting() {
        provider?.onVotingConsist(false)
        view?.showToast("Голосование закончено")
    }

    override fun onCloseButtonClick() {
        deleteUserFromPreferences()
        deleteVotingFromDevice()
        clearAllVoteMan()
        deleteAdmin()
        view?.finishActivity()
    }

    override fun onClickShowDataButton() {
        view?.showCurrentData()
    }

    override fun onViewDetach() {
        view = null
    }
}