package com.elections.voting.presenter.admin

import com.elections.voting.view.admin.IAdminView

interface IAdminPresenter {
    fun onViewAttach(view: IAdminView)
    fun onViewDetach()
    fun onClickFinishVotingButton()
    fun onClickShowDataButton()
    fun onCloseButtonClick()
    fun onClickCreateVote()
    fun onClickCreateVoteWithName(nameVote: String)
    fun onClickStartVoting()
}