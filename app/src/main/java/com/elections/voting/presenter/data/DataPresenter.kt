package com.elections.voting.presenter.data

import com.elections.voting.model.main.IMainControllerCallback
import com.elections.voting.model.main.MainDataController
import com.elections.voting.model.main.MainDataController.Companion.lastPlotsQuery
import com.elections.voting.model.models.VoteData
import com.elections.voting.view.admin.data.IDataView
import java.util.*

class DataPresenter : IDataPresenter, IMainControllerCallback {

    private var view: IDataView? = null
    private var controller: MainDataController? = null
    private var listInSpinner: MutableList<String> = mutableListOf()

    override fun attachView(view: IDataView) {
        this.view = view
        if (controller == null)
            controller = MainDataController(this)
        view.initViews()
        controller?.getMainData()
    }

    override fun onListPlotsResult(list: MutableList<String>) {
        listInSpinner = list

        Collections.sort(list, object : Comparator<String> {
            override fun compare(o1: String, o2: String): Int {
                return extractInt(o1) - extractInt(o2)
            }

            fun extractInt(s: String): Int {
                val num = s.replace("\\D".toRegex(), "")
                return if (num.isEmpty()) 0 else Integer.parseInt(num)
            }
        })

        list.add(0, ALL_PLOTS)
        view?.initSpinner(list)
    }

    override fun onSelectItemsSpinner(strings: MutableList<String>?) {
        if (strings != null)
            lastPlotsQuery = strings
        if (strings?.first() == ALL_PLOTS) {
            controller?.getDataForAllPlots()
            return
        } else
            strings?.let { controller?.getDataForSelectedPlot(it) }
    }

    override fun onClickFindPlot() {
        if (listInSpinner.isNotEmpty()) {
            view?.showDialogSearchPlot()
        }
    }

    override fun onClickFindPlotDialog(query: String) {
        controller?.getDataFromPlot(query)
    }

    override fun onClickRefresh() {
        view?.showProgress()
        onSelectItemsSpinner(lastPlotsQuery)
    }

    override fun onDataFromPlot(data: VoteData) {
        view?.hideProgress()
        view?.setDataInViews(data)
    }

    override fun detachView() {
        view = null
    }

    companion object {
        const val ALL_PLOTS = "По всем участкам"
    }
}