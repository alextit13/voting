package com.elections.voting.presenter.data

import com.elections.voting.view.admin.data.IDataView

interface IDataPresenter {
    fun attachView(view: IDataView)
    fun detachView()
    fun onClickFindPlot()
    fun onClickFindPlotDialog(query: String)
    fun onClickRefresh()
    fun onSelectItemsSpinner(strings: MutableList<String>?)
}