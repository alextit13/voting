package com.elections.voting.presenter.login

import com.elections.voting.view.login.ILoginActivity

interface ILoginPresenter {
    fun attachView(view: ILoginActivity)
    fun detachView()
    fun onClickConfirmButton(login: String, password: String)
    fun onClickInfo()
}