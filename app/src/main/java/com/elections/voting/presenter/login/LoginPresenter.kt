package com.elections.voting.presenter.login

import com.elections.voting.model.database.checkAdmin
import com.elections.voting.model.database.checkSavedUser
import com.elections.voting.model.database.saveAdmin
import com.elections.voting.model.database.saveUserToPreferences
import com.elections.voting.model.network.login.INetworkCallback
import com.elections.voting.model.network.login.INetworkProvider
import com.elections.voting.model.network.login.NetworkProvider
import com.elections.voting.view.login.ILoginActivity

class LoginPresenter : ILoginPresenter, INetworkCallback {

    companion object {
        const val ADMIN_LOGIN = "1"
        const val ADMIN_PASS = "1"
    }

    private var view: ILoginActivity? = null
    private var provider: INetworkProvider? = null

    override fun attachView(view: ILoginActivity) {
        this.view = view
        view.initViews()
        view.initListeners()
        checkAdmin()?.let {
            if (it) {
                view.openAdminActivity()
            }
        }
        checkSavedUser()?.let {
            if (it) view.openMainActivity()
        }
    }

    override fun onClickInfo() {
        view?.showInfoDialog()
    }

    override fun onClickConfirmButton(login: String, password: String) {
        if (login == ADMIN_LOGIN && password == ADMIN_PASS) {
            saveAdmin()
            view?.openAdminActivity()
        }else if (dataIsCorrect(login, password))
            checkValidPair()
        else
            view?.showToast("Введите данные")
    }

    private fun dataIsCorrect(login: String, password: String): Boolean {
        return login.isNotEmpty() && password.isNotEmpty()
    }

    private fun checkValidPair() {
        saveUserToPreferences()
        view?.openMainActivity()
    }

    override fun pairIsCorrect() {

    }

    override fun pairIsIncorrect() {
        view?.showToast("Проверьте введенные данные")
    }

    override fun detachView() {
        view = null
    }
}