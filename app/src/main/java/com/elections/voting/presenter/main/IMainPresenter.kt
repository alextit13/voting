package com.elections.voting.presenter.main

import com.elections.voting.view.main.IMainView

interface IMainPresenter {
    fun onViewAttach(view: IMainView)
    fun onViewDetach()
    fun onClickSendUserData(s1: String, s2: String, s2_1: String)
    fun onClickAddButton(s_1: String, s_2: String, s_2_1: String, s_3: String, s_3_1: String, s_4: String,
        s_4_1: String, s_5: String, s_6: String, s_7: String)

    fun onClickSend_2(
        s_1: String,s_2: String, s_2_1: String, s_3: String, s_3_1: String, s_4: String,
        s_4_1: String, s_5: String, s_6: String, s_7: String
    )

    fun onSelectVotingInSpinner(nameOfVote: String)
    fun onClickInfo()
}