package com.elections.voting.presenter.main

interface IUploadDataToServerCallback {
    fun voteWasComplete()
}