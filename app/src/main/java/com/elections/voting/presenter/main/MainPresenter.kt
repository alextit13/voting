package com.elections.voting.presenter.main

import com.elections.voting.model.database.addVoteMan
import com.elections.voting.model.database.getCurrentVote
import com.elections.voting.model.database.saveNewNameVote
import com.elections.voting.model.database.saveNewVote
import com.elections.voting.model.models.VoteData
import com.elections.voting.model.network.main.IMainDataCallback
import com.elections.voting.model.network.main.MainDataProvider
import com.elections.voting.view.main.IMainView

class MainPresenter(val name: String, val surname: String) : IMainPresenter, IMainDataCallback,
    IUploadDataToServerCallback {

    private var view: IMainView? = null
    private var provider: MainDataProvider? = null

    override fun onViewAttach(view: IMainView) {
        this.view = view
        view.initListeners()
        checkAllDataInSharedPrefs()
    }

    private fun checkAllDataInSharedPrefs() {
        getCurrentVote()?.let { view?.setAllDataInFields(it) }
        getAllDataForSpiner()

        provider?.checkConsistVoting(this)
    }

    private fun getAllDataForSpiner() {
        if (provider == null)
            provider = MainDataProvider()
        provider?.getAllVotings(this)
    }

    override fun onAllVotingsResult(list: MutableList<String>) {
        view?.setDataInSpinner(list)
    }

    override fun onSelectVotingInSpinner(nameOfVote: String) {
        saveNewNameVote(nameOfVote)
        saveNewVote(createNewVoteEmpty())
        provider?.checkConsistVoting(this)
    }

    private fun createNewVoteEmpty(): VoteData {
        return VoteData().apply {
            s_1 = "0"
            s_2 = "0"
            s_2_1 = "0"
            s_3 = "0"
            s_4 = "0"
            s_4_1 = "0"
            s_5 = "0"
            s_5 = "0"
            s_6 = "0"
            s_7 = "0"
            s_fio = "fio"
        }
    }

    override fun onClickInfo() {
        view?.openInfo()
    }

    override fun onValidDataResult(value: Boolean?) {

    }

    override fun onPushFirstDataResultOk() {
        view?.showToast("Данные отправлены!")
    }

    override fun onVoteWasClosed() {
        view?.showToast("Данные не отправлены. Голосование уже закрыто.")
    }

    override fun onClickSendUserData(s1: String, s2: String, s2_1: String) {
        if (s1.isNotEmpty() && s2.isNotEmpty() && s2_1.isNotEmpty()) {
            val currentVote = getCurrentVote()
            currentVote?.s_1 = s1
            currentVote?.s_fio = "$name $surname"
            currentVote?.s_2 = s2
            currentVote?.s_2_1 = s2_1

            currentVote?.let { saveNewVote(it) }

            currentVote.let {
                if (provider == null)
                    provider = MainDataProvider()
                it?.let { it1 ->
                    provider?.pushDataAboutUserToServer(it1, this)
                }
            }

            view?.makeFieldNotClickable()
        } else view?.showToast("Заполните все поля")
    }

    override fun onClickAddButton(
        s_1: String, s_2: String, s_2_1: String, s_3: String, s_3_1: String,
        s_4: String, s_4_1: String, s_5: String, s_6: String, s_7: String
    ) {
        addVoteMan()
        val currentVoteData = getCurrentVote()
        currentVoteData?.apply {
            this.s_1 = s_1
            this.s_fio = "$name $surname"
            this.s_2 = s_2
            this.s_2_1 = s_2_1
            this.s_3 = (this.s_3.toInt() + 1).toString()
            this.s_3_1 = s_3_1
            this.s_4 = (this.s_2.toInt() - this.s_3.toInt()).toString()
            this.s_4_1 = s_4_1
            this.s_5 = s_5
            try {
                this.s_6 = ((this.s_3.toFloat() / this.s_2_1.toFloat()) * 100).toString()
            } catch (e: Exception) {
                this.s_6 = s_6
            }
            try {
                this.s_7 = ((this.s_5.toFloat() / this.s_3.toFloat()) * 100).toString()
            } catch (e: Exception) {
                this.s_7 = s_7
            }
        }

        if (currentVoteData != null)
            saveNewVote(currentVoteData)
        currentVoteData?.let { provider?.pushDataAboutUserToServer(it, this) }
        currentVoteData?.let { view?.setAllDataInFields(it) }
    }

    override fun onClickSend_2(
        s_1: String,
        s_2: String,
        s_2_1: String,
        s_3: String,
        s_3_1: String,
        s_4: String,
        s_4_1: String,
        s_5: String,
        s_6: String,
        s_7: String
    ) {
        val currentVoteData = getCurrentVote()
        val curr_s_2 = currentVoteData?.s_2
        val curr_s_3_1 = s_3_1
        val curr_s_3 = currentVoteData?.s_3
        val curr_s_2_1 = currentVoteData?.s_2_1
        val curr_s_5 = s_5

        try {
            currentVoteData?.s_4_1 = (curr_s_2?.toFloat()?.minus(curr_s_3_1.toFloat())).toString()
            currentVoteData?.s_6 = ((curr_s_3?.toFloat()!! / curr_s_2_1?.toFloat()!!) * 100).toString()
            currentVoteData.s_7 = ((curr_s_5.toFloat() / curr_s_3.toFloat()) * 100).toString()
            currentVoteData.s_3_1 = s_3_1
            currentVoteData.s_5 = s_5
        } catch (e: Exception) {

        }

        currentVoteData?.let { saveNewVote(it) }
        currentVoteData?.let { provider?.pushDataAboutUserToServer(it, this) }
        currentVoteData.let { it?.let { it1 -> view?.setAllDataInFields(it1) } }
        view?.showToast("Данные отправлены")
        view?.blockedAllFields()

        Thread(Runnable {
            var iterator = 30
            view?.showToast("Экран закроется через $iterator секунд")
            while (iterator > 0) {
                iterator--
                Thread.sleep(1000)
                if (iterator == 1)
                    view?.finishCurrentActivity()
            }
        }).start()
    }

    override fun voteWasComplete() {
        view?.showToast("Голосование уже окончено. Ваши данные не будут учтены")
    }

    override fun onPushDataResult() {
        view?.showToast("Данные успешно отправлены")
    }

    override fun onErrorPushData() {
        view?.showToast("Произошла ошибка.")
    }

    override fun onViewDetach() {
        view = null
    }
}
