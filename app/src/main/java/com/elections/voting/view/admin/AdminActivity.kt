package com.elections.voting.view.admin

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import com.elections.voting.R
import com.elections.voting.presenter.admin.AdminPresenter
import com.elections.voting.presenter.admin.IAdminPresenter
import com.elections.voting.view.admin.data.DataActivity
import com.google.firebase.FirebaseApp
import kotlinx.android.synthetic.main.activity_admin.*

class AdminActivity : AppCompatActivity(), IAdminView {

    private var presenter: IAdminPresenter? = null

    private lateinit var titleNameOfVote: TextView
    private lateinit var startVoting: Button
    private lateinit var finishVoting: Button
    private lateinit var showData: Button
    private lateinit var createVote: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FirebaseApp.initializeApp(this)
        val strictMode = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(strictMode)
        setContentView(R.layout.activity_admin)
        attachPresenter()
    }

    private fun attachPresenter() {
        presenter = lastCustomNonConfigurationInstance as? IAdminPresenter
        if (presenter == null)
            presenter = AdminPresenter()
        presenter?.onViewAttach(this)
    }

    override fun initViews() {
        finishVoting = button_finish_voting
        startVoting = button_start_voting
        showData = button_show_data
        createVote = button_create_voting
        titleNameOfVote = text_view_name_of_vote
    }

    override fun initListeners() {
        startVoting.setOnClickListener { presenter?.onClickStartVoting() }
        createVote.setOnClickListener { presenter?.onClickCreateVote() }
        finishVoting.setOnClickListener { presenter?.onClickFinishVotingButton() }
        showData.setOnClickListener { presenter?.onClickShowDataButton() }
    }

    override fun showDialogCreateVote() {
        val editText = EditText(this)
        editText.layoutParams =
            LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        AlertDialog.Builder(this)
            .setView(editText)
            .setTitle("Создание голосования")
            .setMessage("Введите название голосования")
            .setPositiveButton("Создать") { dialog, _ ->
                presenter?.onClickCreateVoteWithName(editText.text.toString())
                dialog?.dismiss()
            }
            .setNegativeButton(
                "Отмена"
            ) { dialog, _ -> dialog?.dismiss() }
            .create().show()
    }

    override fun setNameOfCurrentVoteInTextView(nameVote: String) {
        titleNameOfVote.text = nameVote
    }

    override fun showCurrentData() {
        startActivity(Intent(this, DataActivity::class.java))
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onRetainCustomNonConfigurationInstance(): IAdminPresenter? {
        return presenter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_admin_option, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_admin_close -> presenter?.onCloseButtonClick()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun finishActivity() {
        finish()
    }

    override fun onDestroy() {
        presenter?.onViewDetach()
        presenter = null
        super.onDestroy()
    }
}