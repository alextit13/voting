package com.elections.voting.view.admin

interface IAdminView {
    fun initViews()
    fun initListeners()
    fun showToast(message: String)
    fun showCurrentData()
    fun finishActivity()
    fun showDialogCreateVote()
    fun setNameOfCurrentVoteInTextView(nameVote: String)
}