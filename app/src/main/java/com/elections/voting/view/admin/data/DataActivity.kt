package com.elections.voting.view.admin.data

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.ScrollView
import com.elections.voting.R
import com.elections.voting.model.models.VoteData
import com.elections.voting.presenter.data.DataPresenter
import com.elections.voting.presenter.data.IDataPresenter
import com.elections.voting.view.custom.MultiSelectSpinner
import kotlinx.android.synthetic.main.activity_data.*

class DataActivity : AppCompatActivity(), IDataView, MultiSelectSpinner.OnMultipleItemsSelectedListener {

    private var presenter: IDataPresenter? = null
    private lateinit var spinner: MultiSelectSpinner
    private lateinit var container: ScrollView
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data)
        attachPresenter()
    }

    private fun attachPresenter() {
        presenter = lastNonConfigurationInstance as? IDataPresenter
        if (presenter == null)
            presenter = DataPresenter()
        presenter?.attachView(this)
    }

    override fun initViews() {
        spinner = sp_1
        tv_find_plot.setOnClickListener { presenter?.onClickFindPlot() }
        btn_refresh.setOnClickListener { presenter?.onClickRefresh() }
        container = scroll_view_main_data_container
        progressBar = progress_main_data
        btn_exit.setOnClickListener { finish() }
    }

    override fun showDialogSearchPlot() {
        val editText = EditText(this)
        editText.layoutParams =
            LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        AlertDialog.Builder(this)
            .setView(editText)
            .setTitle("Поиск участка")
            .setMessage("Введите номер участка")
            .setPositiveButton("Поиск") { p0, _ ->
                if (editText.text.toString().isNotEmpty())
                    presenter?.onClickFindPlotDialog(editText.text.toString())
                p0.dismiss()
            }
            .create()
            .show()
    }

    override fun refreshView() {
        recreate()
    }

    override fun selectedIndices(indices: MutableList<Int>?) {
        println("selectedIndices")
    }

    override fun selectedStrings(strings: MutableList<String>?) {
        presenter?.onSelectItemsSpinner(strings)
    }

    override fun showProgress() {
        container.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress(){
        container.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
    }

    override fun initSpinner(listForSpinner: MutableList<String>) {
        spinner.setItems(listForSpinner)
        spinner.setSelection(0)
        spinner.hasNoneOption(true)
        spinner.setListener(this)
    }

    override fun setDataInViews(data: VoteData) {
        et_2.setText(data.s_2)
        et_2_1.setText(data.s_2_1)
        et_3.setText(data.s_3)
        et_3_1.setText(data.s_3_1)
        et_4.setText(data.s_4)
        et_4_1.setText(data.s_4_1)
        et_5.setText(data.s_5)
        et_6.setValue(data.s_6.toFloat())
        et_7.setValue(data.s_7.toFloat())
    }

    override fun onRetainCustomNonConfigurationInstance(): IDataPresenter? {
        return presenter
    }

    override fun onDestroy() {
        presenter?.detachView()
        presenter = null
        super.onDestroy()
    }
}