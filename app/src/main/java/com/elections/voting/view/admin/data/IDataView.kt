package com.elections.voting.view.admin.data

import com.elections.voting.model.models.VoteData

interface IDataView {
    fun initViews()
    fun initSpinner(listForSpinner: MutableList<String>)
    fun setDataInViews(data: VoteData)
    fun showDialogSearchPlot()
    fun refreshView()
    fun showProgress()
    fun hideProgress()
}