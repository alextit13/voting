package com.elections.voting.view.login

interface ILoginActivity {
    fun initViews()
    fun initListeners()
    fun showToast(message: String)
    fun openMainActivity()
    fun openAdminActivity()
    fun showInfoDialog()
}