package com.elections.voting.view.login

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.elections.voting.R
import com.elections.voting.presenter.login.ILoginPresenter
import com.elections.voting.presenter.login.LoginPresenter
import com.elections.voting.view.admin.AdminActivity
import com.elections.voting.view.main.MainActivity
import com.google.firebase.FirebaseApp
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), ILoginActivity {

    private var presenter: ILoginPresenter? = null
    private lateinit var loginEditText: EditText
    private lateinit var passwordEditTxt: EditText
    private lateinit var confirmButton: Button
    private lateinit var infoButton: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        FirebaseApp.initializeApp(this)
        attachPresenter()
    }

    private fun attachPresenter() {
        presenter = lastNonConfigurationInstance as? ILoginPresenter
        if (presenter == null)
            presenter = LoginPresenter()
        presenter?.attachView(this)
    }

    override fun initViews() {
        loginEditText = edit_text_login
        passwordEditTxt = edit_text_password
        confirmButton = button_confirm
        infoButton = text_view_info
    }

    override fun initListeners() {
        confirmButton.setOnClickListener {
            presenter?.onClickConfirmButton(loginEditText.text.toString(), passwordEditTxt.text.toString())
        }
        infoButton.setOnClickListener {
            presenter?.onClickInfo()
        }
    }

    override fun showInfoDialog() {
        val alertDialogBuilder = AlertDialog.Builder(this)
            .setCancelable(false)
            .setTitle("ВХОД")
            .setMessage("Для того что бы получить доступ к системе голосования необходимо узнать логин и пароль у Вашего администратора. Что бы получить информацию по " +
                    "технической части приложения пожалуйста, обратитесь в техническую поддержку сервиса.")
            .setPositiveButton(
                "Понятно"
            ) { dialog, _ -> dialog?.cancel() }
        alertDialogBuilder.create().show()
    }

    override fun openMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("name", loginEditText.text.toString())
        intent.putExtra("surname", passwordEditTxt.text.toString())
        startActivity(intent)
    }

    override fun openAdminActivity() {
        startActivity(Intent(this, AdminActivity::class.java))
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onRetainCustomNonConfigurationInstance(): ILoginPresenter? {
        return presenter
    }

    override fun onDestroy() {
        presenter?.detachView()
        presenter = null
        super.onDestroy()
    }
}