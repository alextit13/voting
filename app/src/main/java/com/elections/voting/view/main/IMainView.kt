package com.elections.voting.view.main

import com.elections.voting.model.models.VoteData

interface IMainView {
    fun initListeners()
    fun clearAllFields()
    fun showToast(message: String)
    fun closeVotings()
    fun addBulleteneToBasket(num: String)
    fun makeFieldNotClickable()
    fun setAllDataInFields(voteData: VoteData)
    fun finishCurrentActivity()
    fun setDataInSpinner(list: MutableList<String>)
    fun setValidData(b: Boolean)
    fun blockedAllFields()
    fun openInfo()
}