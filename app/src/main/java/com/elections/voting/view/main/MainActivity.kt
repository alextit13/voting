package com.elections.voting.view.main

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.elections.voting.R
import com.elections.voting.model.database.deleteAdmin
import com.elections.voting.model.database.deleteUserFromPreferences
import com.elections.voting.model.database.deleteVotingFromDevice
import com.elections.voting.model.models.VoteData
import com.elections.voting.presenter.main.IMainPresenter
import com.elections.voting.presenter.main.MainPresenter
import com.elections.voting.view.InfoActivity
import com.google.firebase.FirebaseApp
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), IMainView {

    private var presenter: IMainPresenter? = null
    private lateinit var spinner: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        FirebaseApp.initializeApp(this)
        attachPresenter()
    }

    private fun attachPresenter() {
        presenter = lastCustomNonConfigurationInstance as? IMainPresenter
        if (presenter == null)
            presenter = MainPresenter(intent.getStringExtra("name"), intent.getStringExtra("surname"))
        presenter?.onViewAttach(this)
    }

    override fun setDataInSpinner(list: MutableList<String>) {
        val spinnerAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list)
        spinner.adapter = spinnerAdapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                presenter?.onSelectVotingInSpinner(list[position])
            }
        }
    }

    override fun openInfo() {
        startActivity(Intent(this, InfoActivity::class.java))
    }

    override fun setValidData(b: Boolean) {
        et_2.isFocusable = b
        et_2_1.isFocusable = b
        et_3.isFocusable = b
        et_3_1.isFocusable = b
        et_4.isFocusable = b
        et_4_1.isFocusable = b
        et_5.isFocusable = b
        et_6.isFocusable = b
        et_7.isFocusable = b
        b_send_1.isFocusable = b

        et_2.isClickable = b
        et_2_1.isClickable = b
        et_3.isClickable = b
        et_3_1.isClickable = b
        et_4.isClickable = b
        et_4_1.isClickable = b
        et_5.isClickable = b
        et_6.isClickable = b
        et_7.isClickable = b
        b_send_1.isClickable = b

        et_2.isFocusableInTouchMode = b
        et_2_1.isFocusableInTouchMode = b
        et_3.isFocusableInTouchMode = b
        et_3_1.isFocusableInTouchMode = b
        et_4.isFocusableInTouchMode = b
        et_4_1.isFocusableInTouchMode = b
        et_5.isFocusableInTouchMode = b
        et_6.isFocusableInTouchMode = b
        et_7.isFocusableInTouchMode = b
        b_send_1.isFocusableInTouchMode = b
    }

    override fun initListeners() {
        b_send_1.setOnClickListener {
            presenter?.onClickSendUserData(
                et_1.text.toString(),
                et_2.text.toString(),
                et_2_1.text.toString()
            )
        }
        b_add.setOnClickListener {
            presenter?.onClickAddButton(
                et_1.text.toString(),
                et_2.text.toString(),
                et_2_1.text.toString(),
                et_3.text.toString(),
                et_3_1.text.toString(),
                et_4.text.toString(),
                et_4_1.text.toString(),
                et_5.text.toString(),
                et_6.text.toString(),
                et_7.text.toString()
            )
        }
        b_send_2.setOnClickListener {
            presenter?.onClickSend_2(
                et_1.text.toString(),
                et_2.text.toString(),
                et_2_1.text.toString(),
                et_3.text.toString(),
                et_3_1.text.toString(),
                et_4.text.toString(),
                et_4_1.text.toString(),
                et_5.text.toString(),
                et_6.text.toString(),
                et_7.text.toString()
            )
        }
        b_send_3.setOnClickListener {
            presenter?.onClickSend_2(
                et_1.text.toString(),
                et_2.text.toString(),
                et_2_1.text.toString(),
                et_3.text.toString(),
                et_3_1.text.toString(),
                et_4.text.toString(),
                et_4_1.text.toString(),
                et_5.text.toString(),
                et_6.text.toString(),
                et_7.text.toString()
            )
        }
        spinner = sp_1
        et_5.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().isNotEmpty()) {
                    val num5 = s.toString().toInt()
                    if (et_3_1.text.toString().isNotEmpty()) {
                        val et31 = et_3_1.text.toString().toInt()
                        if (num5 > et31)
                            Toast.makeText(
                                applicationContext,
                                "Ошибка ввода данных, см. п.3.1",
                                Toast.LENGTH_SHORT
                            ).show()
                    }
                }
            }
        })
        image_view_info.setOnClickListener { presenter?.onClickInfo() }
    }

    override fun makeFieldNotClickable() {
        et_1.isClickable = false
        et_1.isCursorVisible = false
        et_1.isFocusable = false
        et_1.isFocusableInTouchMode = false

        et_2.isClickable = false
        et_2.isCursorVisible = false
        et_2.isFocusable = false
        et_2.isFocusableInTouchMode = false

        et_2_1.isClickable = false
        et_2_1.isCursorVisible = false
        et_2_1.isFocusable = false
        et_2_1.isFocusableInTouchMode = false
    }

    override fun setAllDataInFields(voteData: VoteData) {
        et_1.setText(if (voteData.s_1 != "0") voteData.s_1 else "")
        et_2.setText(if (voteData.s_2 != "0") voteData.s_2 else "")
        et_2_1.setText(if (voteData.s_2_1 != "0") voteData.s_2_1 else "")
        et_3.setText(if (voteData.s_3 != "0") voteData.s_3 else "")
        et_3_1.setText(if (voteData.s_3_1 != "0") voteData.s_3_1 else "")
        et_4.setText(if (voteData.s_4 != "0") voteData.s_4 else "")
        et_4_1.setText(if (voteData.s_4_1 != "0") voteData.s_4_1 else "")
        et_5.setText(if (voteData.s_5 != "0") voteData.s_5 else "")
        et_6.setText(if (voteData.s_6 != "0") voteData.s_6 else "")
        et_7.setText(if (voteData.s_7 != "0") voteData.s_7 else "")
    }

    override fun clearAllFields() {

    }

    override fun blockedAllFields() {
        b_send_1.isClickable = false
        b_send_1.isCursorVisible = false
        b_send_1.isFocusable = false
        b_send_1.isFocusableInTouchMode = false

        b_add.isClickable = false
        b_add.isCursorVisible = false
        b_add.isFocusable = false
        b_add.isFocusableInTouchMode = false

        b_send_2.isClickable = false
        b_send_2.isCursorVisible = false
        b_send_2.isFocusable = false
        b_send_2.isFocusableInTouchMode = false
    }

    override fun finishCurrentActivity() {
        deleteAdmin()
        deleteUserFromPreferences()
        deleteVotingFromDevice()
        runOnUiThread { finish() }
    }

    override fun addBulleteneToBasket(num: String) {
        et_3.setText(num)
    }

    override fun showToast(message: String) {
        runOnUiThread { Toast.makeText(this, message, Toast.LENGTH_LONG).show() }
    }

    override fun closeVotings() {
        finish()
    }

    override fun onRetainCustomNonConfigurationInstance(): IMainPresenter? {
        return presenter
    }

    override fun onDestroy() {
        presenter?.onViewDetach()
        presenter = null
        super.onDestroy()
    }
}
